# In this at the end of filevector I am putting the dirname
# so loop over n-1 files and n will give the name of the output dir.
# In legend also the n element will give the name for the ratio plot y axis label.
#edited by Monika Mittal 
#Script for ratio plot 
#import sys
#sys.argv.append( '-b-' )
import ROOT
ROOT.gROOT.SetBatch(True)

#import ROOT
from ROOT import TFile, TH1F, gDirectory, TCanvas, TPad, TProfile,TGraph, TGraphAsymmErrors, TColor
from ROOT import TH1D, TH1, TH1I, TH2F
from ROOT import gStyle
from ROOT import gROOT
from ROOT import TStyle
from ROOT import TLegend
from ROOT import TMath
from ROOT import TPaveText
from ROOT import TLatex



import os
colors=[1,2,4,3,32,20,6,8,20,11,41,46,30,12,28,20,32]
markerStyle=[20,21,22,23,24,25,26,27,28,29,20,21,22,23]            
linestyle=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
## logstatus = x,y,text
def DrawOverlap(fileVec, histVec, titleVec,legendtext,pngname,logstatus=[0,0,0],xRange=[-99999,99999,1],b_axislabel =0):

    gStyle.SetOptTitle(0)
    gStyle.SetOptStat(0)
    gStyle.SetTitleOffset(1.1,"Y");
    gStyle.SetTitleOffset(0.9,"X");
    gStyle.SetLineWidth(3)
    gStyle.SetFrameLineWidth(3); 
    gStyle.SetPaintTextFormat("5.2f");
#    gStyle.SetPalette(kRainBow);
    gStyle.SetPalette(55);
    i=0

    histList_=[]
    histList=[]
    histList1=[]
    maximum=[]
    
    ## Legend    
    leg = TLegend(0.1, 0.70, 0.89, 0.89)#,NULL,"brNDC");
    leg.SetBorderSize(0)
    leg.SetNColumns(2)
    leg.SetLineColor(1)
    leg.SetLineStyle(1)
    leg.SetLineWidth(1)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextFont(22)
    leg.SetTextSize(0.045)
     
    c = TCanvas("c1", "c1",0,0,1200,800)
    #c.SetBottomMargin(0.15)
    #c.SetLeftMargin(0.15)
    #c.SetLogy(0)
    #c.SetLogx(0)
    c1_2 = TPad("c1_2","newpad",0.02,0.02,0.98,0.994)
    c1_2.Draw()

    
    print ("you have provided "+str(len(fileVec))+" files and "+str(len(histVec))+" histograms to make a overlapping plot" )
    print "opening rootfiles"
    c.cd()
    c1_2.SetBottomMargin(0.13)
    c1_2.SetLogy(logstatus[1])
    c1_2.SetLogx(logstatus[0])
#    c1_2.SetLogz(1)
    
    c1_2.cd()
    ii=0    
    inputfile={}
    print str(fileVec[(len(fileVec)-1)])
    print str(histVec[(len(histVec)-1)])
    for ifile_ in range(len(fileVec)-1):
        print ("opening file  "+fileVec[ifile_])
        inputfile[ifile_] = TFile( fileVec[ifile_] )
        print "fetching histograms"
        for ihisto_ in range(len(histVec)-1):
            
            print ("printing histo "+str(histVec[ihisto_]))
            histo = inputfile[ifile_].Get(histVec[ihisto_])
            histo_nevents = inputfile[ifile_].Get("hm_events")
            histo_lumi = inputfile[ifile_].Get("hm_lumi")
            print type(histo_lumi)
            histList.append(histo)
            histList1.append(histo)
            histoname_= histo.GetName()
            print histo_nevents.GetBinContent(1)
            print ( 'Integral   :'  , histo.Integral())
            print ('find----', histoname_.find("hitrateVsLumi_chamber_"), histoname_)
            if ( (histoname_ == "hm_time") or (histoname_ == "hm_etaphi") or (histoname_ == "hm_adc") or (histoname_ == "hm_rz") or (histoname_ == "hm_lumi")):
                print histoname_



            elif("hitrateVsLumi_chamber_" in histoname_):
                histList[ii].Divide(histo_lumi)                
                print " i am here"



            else :
                histList[ii].Scale(1.0/histo_nevents.GetBinContent(1))

          

            type_obj = type(histList[ii])
            if (type_obj is TH1D) or (type_obj is TH1F) or (type_obj is TH1) or (type_obj is TH1I) :
                histList[ii].Rebin(xRange[2])
               # histList[ii].Scale(1.0/histList[ii].Integral())
                maximum.append(histList[ii].GetMaximum())
                maximum.sort()
            ii=ii+1

    print histList
    for ih in range(len(histList)):
        tt = type(histList[ih])
        if ( (tt is TGraphAsymmErrors ) or (tt is TGraph) or (tt is TH1D) or (tt is TH1F) or (tt is TH1) or (tt is TH1I) ):
            if logstatus[1] is 1 :
                histList[ih].SetMaximum(maximum[(len(maximum)-1)]*25) #1.4 for log
                histList[ih].SetMinimum(1) #1.4 for log
            if logstatus[1] is 0 :
                histList[ih].SetMaximum(maximum[(len(maximum)-1)]*1.2) #1.4 for log
                histList[ih].SetMinimum(0) #1.4 for log
        if ih == 0 :      
            if (tt is TGraphAsymmErrors) | (tt is TGraph) : 
                histList[ih].Draw("AL3")
            if (tt is TH1D) or (tt is TH1F) or (tt is TH1) or (tt is TH1I) :

                histList[ih].SetMarkerSize(1.8)

                histList[ih].Draw("hist text45 ")
                if(logstatus[2] is  1) :
                    histList[ih].Draw("hist ")   
                if(logstatus[2] is  2) :
                    histList[ih].Draw("P ")   
                    histList[ih].SetMarkerStyle(markerStyle[ih])
               
            if (tt is TH2F):
                histList[ih].SetMarkerSize(1.8)

                histList[ih].Draw("colz text45") 

                if(logstatus[2] is  1) :
                    histList[ih].Draw("colz ")     


        if ih > 0 :
            #histList[ih].SetLineWidth(2)
            if (tt is TGraphAsymmErrors) | (tt is TGraph) : 
                histList[ih].Draw("L3 same")
            if (tt is TH1D) or (tt is TH1F) or (tt is TH1) or (tt is TH1I) :
                histList[ih].Draw(" hist same")  
            if (tt is TH2F) :
                histList[ih].SetMarkerSize(1.8)
                histList[ih].Draw("colz same")     
        if (tt is TGraphAsymmErrors) | (tt is TGraph) :
            histList[ih].SetMaximum(10000.0) 
            histList[ih].SetMinimum(0.0001) 
            histList[ih].SetMarkerColor(colors[ih])
            histList[ih].SetLineColor(colors[ih])
            histList[ih].SetLineWidth(3)
            histList[ih].SetLineStyle(linestyle[ih])
            

            leg.AddEntry(histList[ih],legendtext[ih],"PL")
        if (tt is TH1D) or (tt is TH1F) or (tt is TH1) or (tt is TH1I) :
            histList[ih].SetLineStyle(linestyle[ih])
            histList[ih].SetLineColor(colors[ih])
            histList[ih].SetLineWidth(3)
            #leg.AddEntry(histList[ih],legendtext[ih],"L")
        histList[ih].GetYaxis().SetTitle(titleVec[1])
        histList[ih].GetYaxis().SetTitleSize(0.052)
        histList[ih].GetYaxis().SetTitleOffset(0.88)
        histList[ih].GetYaxis().SetTitleFont(22)
        histList[ih].GetYaxis().SetLabelFont(22)
        histList[ih].GetYaxis().SetLabelSize(.052)
        histList[ih].GetXaxis().SetRangeUser(xRange[0],xRange[1])
#        histList[ih].GetZaxis().SetRangeUser(0.1,1)
        histList[ih].GetXaxis().SetLabelSize(0.0000);
        histList[ih].GetXaxis().SetTitle(titleVec[0])
        histList[ih].GetXaxis().SetLabelSize(0.052)
        histList[ih].GetXaxis().SetTitleSize(0.052)
        histList[ih].GetXaxis().SetTitleOffset(1.04)
        histList[ih].GetXaxis().SetTitleFont(22)
        histList[ih].GetXaxis().SetTickLength(0.07)
        histList[ih].GetXaxis().SetLabelFont(22)
        histList[ih].GetYaxis().SetLabelFont(22) 
        histList[ih].GetZaxis().SetTitle("Rate [Hz/cm^{2}]")
        histList[ih].GetZaxis().SetTitleSize(0.052)
        histList[ih].GetZaxis().SetTitleOffset(0.67)
        histList[ih].GetZaxis().SetTitleFont(22)
        histList[ih].GetZaxis().SetLabelFont(22)  
        histList[ih].GetZaxis().SetLabelSize(.052)  
        histList[ih].SetMarkerSize(1.8)
        histList[ih].SetStats(0)


            ### Summary Plot
        if ih is 0 :
            if str(histVec[ihisto_]) is "hm_summary_L" : 
                c1_2.SetLogz(1)
                for nbiny in range(histList[0].GetNbinsY()): 
                    histList[0].GetYaxis().SetBinLabel(1,"BIL")
                    histList[0].GetYaxis().SetBinLabel(2,"BML")
                    histList[0].GetYaxis().SetBinLabel(3,"BOL")
                    histList[0].GetYaxis().SetBinLabel(4,"EIL")
                    histList[0].GetYaxis().SetBinLabel(5,"EML")
                    histList[0].GetYaxis().SetBinLabel(6,"EOL")
                    histList[0].GetYaxis().SetBinLabel(7,"EEL")
                    histList[ih].GetYaxis().SetLabelSize(.062)
                    histList[ih].GetYaxis().SetTitleOffset(1.0)
                    histList[ih].GetYaxis().SetTickSize(0.00)


            if str(histVec[ihisto_]) is "hm_summary_S" : 
                c1_2.SetLogz(1)
                for nbiny in range(histList[0].GetNbinsY()): 
                    histList[0].GetYaxis().SetBinLabel(1,"BIS")
                    histList[0].GetYaxis().SetBinLabel(2,"BMS")
                    histList[0].GetYaxis().SetBinLabel(3,"BOS")
                    histList[0].GetYaxis().SetBinLabel(4,"EIS")
                    histList[0].GetYaxis().SetBinLabel(5,"EMS")
                    histList[0].GetYaxis().SetBinLabel(6,"EOS")
                    histList[0].GetYaxis().SetBinLabel(7,"EES")
                    for nbins in range(1,18):
                        histList[0].SetBinContent(nbins,4,0)
                    

                    histList[ih].GetYaxis().SetLabelSize(.062)
                    histList[ih].GetYaxis().SetTitleOffset(1.0)
                    histList[ih].GetYaxis().SetTickSize(0.00)
# histList[ih].GetXaxis().SetNdivisions(508)
#

        i=i+1
    pt = TPaveText(0.0877181,0.9,0.9580537,0.96,"brNDC")
    pt.SetBorderSize(0)
    pt.SetTextAlign(12)
    pt.SetFillStyle(0)
    pt.SetTextFont(22)
    pt.SetTextSize(0.046)
    text = pt.AddText(0.05,0.5,"ATLAS Internal         Run 311287")
    #text = pt.AddText(0.5,0.5,"12.9 fb^{-1} (13 TeV)")
    text = pt.AddText(0.8,0.5," (13 TeV)")
    text = pt.AddText(0.60,0.5,"Empty")
    pt.Draw()
   
    

#    t2a = TPaveText(0.0877181,0.81,0.9580537,0.89,"brNDC")
#    t2a.SetBorderSize(0)
#    t2a.SetFillStyle(0)
#    t2a.SetTextSize(0.040) 
#    t2a.SetTextAlign(12)
#    t2a.SetTextFont(62)
#    histolabel1= str(fileVec[(len(fileVec)-1)])
#    text1 = t2a.AddText(0.06,0.5,"CMS Internal") 
#    t2a.Draw()
    leg.Draw()
#
#    c.cd()

    c1_2.Modified()
    c1_2.Update()
    outputdir_dir = fileVec[len(fileVec)-1]
    print outputdir_dir
#    outputdirname = outputdir_dir + '/'+ "MDT" +'/'
    outputdirname = outputdir_dir + '/'+ histVec[(len(histVec)-1)] +'/'
    print (outputdirname)
    if not os.path.exists(outputdirname):
        os.makedirs(outputdirname)
    histname=outputdirname+pngname 
    c.SaveAs(histname+'.png')
    c.SaveAs(histname+'.pdf')
    c.SaveAs(histname+'.root')
    outputname = 'cp  -r '+ outputdir_dir +' /afs/cern.ch/work/m/mmittal/public/'
    ex1 = 'cp /afs/cern.ch/work/m/mmittal/public/index.php /afs/cern.ch/work/m/mmittal/public/'+outputdir_dir
    ex = 'cp /afs/cern.ch/work/m/mmittal/public/index.php /afs/cern.ch/work/m/mmittal/public/'+outputdirname
    os.system(outputname) 
    os.system(ex1) 
    os.system(ex) 





print "calling the plotter"
dirname_='08032018'
runnumber_='n010e.00311287' #'00309314'



InputFle =[dirname_+'/Merged_'+runnumber_+'.root','New_'+dirname_+'_'+runnumber_]


DrawOverlap(InputFle,['hm_etaphi_BMS','MDT/BMS'],["#eta station","#phi station"],[''],'etaphi_BMS')

DrawOverlap(InputFle,['hm_summary_L','MDT'],["#eta station"," chambers "],[''],'hm_summary_L')

DrawOverlap(InputFle,['hm_summary_S','MDT'],["#eta station"," chambers "],[''],'hm_summary_S')

DrawOverlap(InputFle,['hm_time','MDT'],["time[ns]","#hits"],[''],'driftTime',[0,0,1],[-400,1400,1])
DrawOverlap(InputFle,['hm_rz','MDT'],["z(m)","R(m)"],[''],'rz_totalMDT',[0,0,1])
DrawOverlap(InputFle,['hm_adc','MDT'],["adc","hits"],[''],'adc',[0,0,1],[0,500,1])
DrawOverlap(InputFle,['hm_summary_L','MDT'],["#eta station"," chambers "],[''],'hm_summary_L')
DrawOverlap(InputFle,['hm_summary_S','MDT'],["#eta station"," chambers "],[''],'hm_summary_S')
DrawOverlap(InputFle,['hm_lumi','MDT'],["luminosity"," #events "],[''],'hm_lumi')

DrawOverlap(InputFle,['hm_etaphi_BIS','MDT/BIS'],["#eta station","#phi station"],[''],'etaphi_BIS')


DrawOverlap(InputFle,['hm_etaphi_BOS','MDT/BOS'],["#eta station","#phi station"],[''],'etaphi_BOS')
DrawOverlap(InputFle,['hm_etaphi_BIL','MDT/BIL'],["#eta station","#phi station"],[''],'etaphi_BIL')
DrawOverlap(InputFle,['hm_etaphi_BML','MDT/BML'],["#eta station","#phi station"],[''],'etaphi_BML')
DrawOverlap(InputFle,['hm_etaphi_BOL','MDT/BOL'],["#eta station","#phi station"],[''],'etaphi_BOL')
DrawOverlap(InputFle,['hm_etaphi_EIS','MDT/EIS'],["#eta station","#phi station"],[''],'etaphi_EIS')
DrawOverlap(InputFle,['hm_etaphi_EMS','MDT/EMS'],["#eta station","#phi station"],[''],'etaphi_EMS')
DrawOverlap(InputFle,['hm_etaphi_EOS','MDT/EOS'],["#eta station","#phi station"],[''],'etaphi_EOS')
DrawOverlap(InputFle,['hm_etaphi_EIL','MDT/EIL'],["#eta station","#phi station"],[''],'etaphi_EIL')
DrawOverlap(InputFle,['hm_etaphi_EML','MDT/EML'],["#eta station","#phi station"],[''],'etaphi_EML')
DrawOverlap(InputFle,['hm_etaphi_EOL','MDT/EOL'],["#eta station","#phi station"],[''],'etaphi_EOL')
DrawOverlap(InputFle,['hm_etaphi_EEL','MDT/EEL'],["#eta station","#phi station"],[''],'etaphi_EEL')
DrawOverlap(InputFle,['hm_etaphi_EES','MDT/EES'],["#eta station","#phi station"],[''],'etaphi_EES')

DrawOverlap(InputFle,['hm_eta_BIS','MDT/BIS'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_BIS')
DrawOverlap(InputFle,['hm_eta_BMS','MDT/BMS'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_BMS')
DrawOverlap(InputFle,['hm_eta_BOS','MDT/BOS'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_BOS')
DrawOverlap(InputFle,['hm_eta_BIL','MDT/BIL'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_BIL')
DrawOverlap(InputFle,['hm_eta_BML','MDT/BML'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_BML')
DrawOverlap(InputFle,['hm_eta_BOL','MDT/BOL'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_BOL')
DrawOverlap(InputFle,['hm_eta_EIS','MDT/EIS'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_EIS')
DrawOverlap(InputFle,['hm_eta_EMS','MDT/EMS'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_EMS')
DrawOverlap(InputFle,['hm_eta_EOS','MDT/EOS'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_EOS')
DrawOverlap(InputFle,['hm_eta_EIL','MDT/EIL'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_EIL')
DrawOverlap(InputFle,['hm_eta_EML','MDT/EML'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_EML')
DrawOverlap(InputFle,['hm_eta_EOL','MDT/EOL'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_EOL')
DrawOverlap(InputFle,['hm_eta_EEL','MDT/EEL'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_EEL')
DrawOverlap(InputFle,['hm_eta_EES','MDT/EES'],["#eta","Hit rate [Hz/cm^{2}]"],[''],'eta_EES')

DrawOverlap(InputFle,['hitrateVsZ_BIS','MDT'],["|z| cm","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_BIS')
DrawOverlap(InputFle,['hitrateVsZ_BMS','MDT'],["|z| cm","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_BMS')
DrawOverlap(InputFle,['hitrateVsZ_BOS','MDT'],["|z| cm","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_BOS')
DrawOverlap(InputFle,['hitrateVsZ_BIL','MDT'],["|z| cm","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_BIL')
DrawOverlap(InputFle,['hitrateVsZ_BML','MDT'],["|z| cm","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_BML')
DrawOverlap(InputFle,['hitrateVsZ_BOL','MDT'],["|z| cm","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_BOL')
DrawOverlap(InputFle,['hitrateVsZ_EIS','MDT'],["r(cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EIS')
DrawOverlap(InputFle,['hitrateVsZ_EMS','MDT'],["r(cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EMS')
DrawOverlap(InputFle,['hitrateVsZ_EOS','MDT'],["r(cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EOS')
DrawOverlap(InputFle,['hitrateVsZ_EIL','MDT'],["r(cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EIL')
DrawOverlap(InputFle,['hitrateVsZ_EML','MDT'],["r(cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EML')
DrawOverlap(InputFle,['hitrateVsZ_EOL','MDT'],["r(cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EOL')
DrawOverlap(InputFle,['hitrateVsZ_EEL','MDT'],["r(cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EEL')
DrawOverlap(InputFle,['hitrateVsZ_EES','MDT'],["r(cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EES')

###
#
#DrawOverlap(InputFle,['hitrateVsZ_BI','MDT'],["|z| (cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_BI')
#DrawOverlap(InputFle,['hitrateVsZ_BM','MDT'],["|z| (cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_BM')
#DrawOverlap(InputFle,['hitrateVsZ_BO','MDT'],["|z| (cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_BO')
#DrawOverlap(InputFle,['hitrateVsZ_EI','MDT'],["|z| (cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EI')
#DrawOverlap(InputFle,['hitrateVsZ_EM','MDT'],["|z| (cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EM')
#DrawOverlap(InputFle,['hitrateVsZ_EO','MDT'],["|z| (cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EO')
#DrawOverlap(InputFle,['hitrateVsZ_EE','MDT'],["|z| (cm)","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsZ_EE')
#

###
DrawOverlap(InputFle,['hitrateVsLumi_chamber_BIS','MDT/BIS'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_BIS',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_BMS','MDT/BMS'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_BMS',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_BOS','MDT/BOS'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_BOS',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_BIL','MDT/BIL'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_BIL',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_BML','MDT/BML'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_BML',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_BOL','MDT/BOL'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_BOL',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_EIS','MDT/EIS'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_EIS',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_EMS','MDT/EMS'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_EMS',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_EOS','MDT/EOS'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_EOS',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_EIL','MDT/EIL'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_EIL',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_EML','MDT/EML'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_EML',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_EOL','MDT/EOL'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_EOL',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_EEL','MDT/EEL'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_EEL',[0,0,2],[0,13000,1])
DrawOverlap(InputFle,['hitrateVsLumi_chamber_EES','MDT/EES'],["Inst. lumi (e^{30}cm^{-2}s^{-1})","Hit rate [Hz/cm^{2}]"],[''],'hitrateVsLumi_chamber_EES',[0,0,2],[0,13000,1])



