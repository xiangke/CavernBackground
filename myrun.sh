#!/usr/bin/env bash

shopt -s expand_aliases
#export RUCIO_ACCOUNT=xiangke  #set rucio account
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
#setupATLAS

kinit < /afs/cern.ch/user/x/xiangke/.globus/pass
eosfusebind

CURRDIR=/afs/cern.ch/work/x/xiangke/Cavernbgk/muTrigNt_read
cd $CURRDIR

source /afs/cern.ch/work/x/xiangke/Cavernbgk/muTrigNt_read/scripts/setup_area.sh

# run the code
dirname=TestCondor

inputfile=$1
outputfile=`basename ${inputfile}`
job=testcondor

if [ ! -d $dirname/${job} ]; then
	mkdir -p $dirname/${job}
fi

cd $dirname/$job
if [ -f $outputfile ]; then
	rm $outputfile
fi
run_example_looper -i dcache:${inputfile} -c ex -o ${outputfile}
if [ $? -eq 0 ]; then
	echo 'job finished'
else
	echo 'job failed'
fi
