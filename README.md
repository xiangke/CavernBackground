CavernBackground
================


A package to prepare a working area for running the code for the HitRate that depend upon Ntuples produced by 
**PhaseIIMuonTriggerNtuples**  a package for trigger upgrade [here]( https://gitlab.cern.ch/atlas_hllhc_muon_trigger ).
The Package**muTrigNt_write** which is used for writing muon trigger ntuple,it have been changed from cmt to cmake, the version used is [here.](https://gitlab.cern.ch/atlas_hllhc_muon_trigger/muTrigNt_write/tree/cmt_to_cmake)  The Package **muTrigNt_read**  which is used for reading **PhaseIIMuonTriggerNtuples** and 
**MuTrigNtExampleAna** that illustrate how to read and loop over PhaseIIMuonTriggerNtuples
 has been modified according to the needs and below describes various steps how to conquer it

Prerequisites:
-------------
svn access and kerberos ticket (klist)

    Run kinit beforehand if no kerberos ticket is established (you will be asked for your CERN password when running setup_area.sh otherwise)

access to cvmfs (for setting up an AnalysisRelease)  

To read PhaseIIMuonTriggerNtuples
---------------------------------

Follow these commands to set up an area to read PhaseIIMuonTriggerNtuples:

1) Step:
```
mkdir CavernBackground
cd CavernBackground
git clone ssh://git@gitlab.cern.ch:7999/atlas_hllhc_muon_trigger/muTrigNt_read.git
cd muTrigNt_read
source scripts/install.sh --tag nXXX |& tee install.log  # replace `XXX` with tag number # current one n010
source scripts/setup_area.sh --clean |& tee setup_area.log
```
where nXXX is PhaseIIMuonTriggerNtuple tag (e.g. n010 is the latest one) that you want to read.

install.sh checks out PhaseIIMuonTriggerNtuple as well as the packages that it depends on.

setup_area.sh sets up the needed ROOT version as well as well as RootCore and (with the --clean option) compiles all of the packages.


To checkout the necessary packages used for the production nXXX you must provide to the install.sh script the --tag option so that it will checkout the version of PhaseIIMuonTriggerNtuple and its dependencies that were used at the time of producing the nXXX PhaseIIMuonTriggerNtuple.







It depend upon two more packages ++RPCRoi++ abd ++TgcRoi


2) Step:

```
git clone ssh://git@gitlab.cern.ch:7999/atlas_hllhc_muon_trigger/RpcRoi.git
git clone ssh://git@gitlab.cern.ch:7999/atlas_hllhc_muon_trigger/TgcRoi.git
rc find_packages
rc compile_pkg RpcRoi
rc compile_pkg TgcRoi
```


If package ++TgcRoi++ doesnt compile comment out the line No L53 and L526 (.z information) in file TgcRoi/Root/TgcTracking.cxx



3)Step:

```
git clone ssh://git@gitlab.cern.ch:7999/mmittal/CavernBackground.git
scp -r CavernBackground/* .
rm -rf CavernBackground
rc find_packages
rc compile_pkg MuTrigNtExampleAna
```


Follow the Step1,2 and 3 It will install and compile  all the required packages.


To resume the environment (set up ROOT and RootCore) in subseqent sessions, call
```
source scripts/setup_area.sh
```

Add the --clean option (source scripts/setup_area.sh --clean) to refresh RootCore.



To run the example looper code over a PhaseIIMuonTriggerNtuple:
---------------------------------------------------------------
```    
run_example_looper -i [ROOT file or directory of ROOT files] -c [zb or ex] -o [output root files]
```

To add your own functionality to the looper, edit Root/ExampleLooper.cxx

To modify the executable, edit util/run_example_looper.cxx

Don't forget to compile after making changes: rc compile_pkg MuTrigNtExampleAna


Updating to a new tag

 recommend creating a new work area (i.e. directory) for each tag that you use, e.g.
```
mkdir nXXX  # replace `XXX` with tag number
git clone ssh://git@gitlab.cern.ch:7999/atlas_hllhc_muon_trigger/muTrigNt_read.git
 remaining commands above for setting up an area
```
Using RootCore

Common RootCore commands are
```
rc find_packages  # call anytime a RootCore package is added or removed
rc clean          # clean all RootCore packages
rc compile        # compile all RootCore packages
```
To clean or compile a single RootCore package, use

```
rc clean_pkg <package>    # clean single RootCore package
rc compile_pkg <package>  # compile single RootCore package
For more information about RootCore, see https://twiki.cern.ch/twiki/bin/view/AtlasComputing/RootCore
```
To Submit batch jobs
--------------------
```
condor_submit mycondor.conf

```
Modify according to needs for the runumber used

To produce plots
----------------
```
python Histograms_FromRootile_norm.py
root -l -b -q CompLumi.C
root -l -b -q fourCom.C
root -l -b -q driftime.C
```

