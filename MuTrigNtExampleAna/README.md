MuTrigNtExampleAna
==================

Package to illustrate how to read and loop over PhaseIIMuonTriggerNtuples.


Instructions:
-------------

1. Install (if you didn't already) **muTrigNt_read**, following the instructions [there](https://gitlab.cern.ch/atlas_hllhc_muon_trigger/muTrigNt_read).
    + This will install RootCore and enable the `rc` command.

2. If you returning and need to resume your previous muTrigNt_read environment, in the muTrigNt_read directory run
        ```
        source scripts/setup_area.sh
        ```

3. In the muTrigNt_read directory, clone this package:
        ```
        git clone ssh://git@gitlab.cern.ch:7999/atlas_hllhc_muon_trigger/MuTrigNtExampleAna.git
        ```

4. Tell RootCore about this package:
        ```
        rc find_packages
        ```

5. Compile with
        ```
        rc compile_pkg MuTrigNtExampleAna
        ```

* To run the example looper code over a PhaseIIMuonTriggerNtuple:
        ```
        run_example_looper -i [ROOT file or directory of ROOT files]
        ```

* To get a list of options:
        ```
        run_example_looper --help
        ```

* To add your own functionality to the looper, edit **Root/ExampleLooper.cxx**

* To modify the executable, edit **util/run_example_looper.cxx**

* Don't forget to compile after making changes: `rc compile_pkg MuTrigNtExampleAna`

