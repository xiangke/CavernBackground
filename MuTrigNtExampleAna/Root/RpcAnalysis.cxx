#include <iostream>
#include <bitset>
#include <cmath>
#include <TMath.h>
#include <TFile.h>
#include "MuTrigNtExampleAna/RpcAnalysis.h"

RpcAnalysis::RpcAnalysis(){}

RpcAnalysis::~RpcAnalysis(){}

void RpcAnalysis::initialize(){


  ntests = 6;      // 6 quality values
  nthresholds = 2; // 2 pt thresholds
  threshold.clear();
  threshold.push_back(10.0);
  threshold.push_back(20.0);
  
  //TH1::SetDefaultSumw2();

  h_events = new TH1F("h_events","h_events",12,0,12);
  h_tower_vs_eta = new TH2F("h_tower_vs_eta","h_tower_vs_eta",150,-1.5,1.5,22,0,22);
  h_sector_vs_phi = new TH2F("h_sector_vs_phi","h_sector_vs_phi",128,-M_PI,M_PI,32,0,32);
  
  h_oktrks_pt = new TH1F("h_oktrks_pt","h_oktrks_pt",200,0,100);
  h_oktrks_eta = new TH1F("h_oktrks_eta","h_oktrks_eta",110,-1.1,1.1);
  h_oktrks_etaphi = new TH2F("h_oktrks_etaphi","h_oktrks_etaphi",110,-1.1,1.1,128,-TMath::Pi(),TMath::Pi());


  
  h_oktrks_pt_trig_old = new TH1F("h_oktrks_pt_trig_old","h_oktrks_pt_trig_old",200,0,100);
  h_oktrks_eta_trig_old = new TH1F("h_oktrks_eta_trig_old","h_oktrks_eta_trig_old",110,-1.1,1.1);
  h_oktrks_etaphi_trig_old = new TH2F("h_oktrks_etaphi_trig_old","h_oktrks_etaphi_trig_old",110,-1.1,1.1,128,-TMath::Pi(),TMath::Pi());
  
  h_oktrks_pt_w = new TH1F("h_oktrks_pt_w","h_oktrks_pt_w",200,0,100);
  h_oktrks_etaphi_w = new TH2F("h_oktrks_etaphi_w","h_oktrks_etaphi_w",110,-1.1,1.1,128,-TMath::Pi(),TMath::Pi());

  h_time = new TH1F("h_time","h_time",200,-100,100);
  h_time_BIBO = new TH1F("h_time_BIBO","h_time_BIBO",200,-100,100);
  h_time_BI = new TH1F("h_time_BI","h_time_BI",200,-100,100);

  // assign empty pointers to histogram vectors
  std::vector<TH1F*> tmp1(ntests,0);
  std::vector<TH2F*> tmp2(ntests,0);
  h_patterns.assign(nthresholds,0);



  h_oktrks_pt_trig.assign(nthresholds, tmp1);
  h_oktrks_eta_trig.assign(nthresholds, tmp1);
  h_oktrks_etaphi_trig.assign(nthresholds, tmp2);
  h_oktrks_pt_trig_w.assign(nthresholds, tmp1);
  h_oktrks_etaphi_trig_w.assign(nthresholds, tmp2);
  h_eta_vs_phi_pattern.assign(nthresholds, tmp2);
  
  char hname[255];
  for (int j=0; j<nthresholds; j++){
      sprintf(hname,"h_patterns_th_%d",j);
      h_patterns.at(j) =  new TH1F(hname,hname,ntests,0.,(float)ntests);
      
      for (int i=0; i<ntests; i++){
          sprintf(hname,"h_oktrks_pt_trig_%d_th_%d",i,j);
          h_oktrks_pt_trig.at(j).at(i) = new TH1F(hname,hname,200,0,100);
          sprintf(hname,"h_oktrks_eta_trig_%d_th_%d",i,j);
          h_oktrks_eta_trig.at(j).at(i) = new TH1F(hname,hname,110,-1.1,1.1);
          sprintf(hname,"h_oktrks_etaphi_trig_%d_th_%d",i,j);
          h_oktrks_etaphi_trig.at(j).at(i) = new TH2F(hname,hname,110,-1.1,1.1,128,-TMath::Pi(),TMath::Pi());
          sprintf(hname,"h_oktrks_pt_trig_%d_th_%d_w",i,j);
          h_oktrks_pt_trig_w.at(j).at(i) = new TH1F(hname,hname,200,0,100);
          sprintf(hname,"h_oktrks_etaphi_trig_%d_th_%d_w",i,j);
          h_oktrks_etaphi_trig_w.at(j).at(i) = new TH2F(hname,hname,110,-1.1,1.1,128,-TMath::Pi(),TMath::Pi());

          sprintf(hname,"h_eta_vs_phi_pattern_%d_th_%d",i,j);
          h_eta_vs_phi_pattern.at(j).at(i) = new TH2F(hname,hname,110,-1.1,1.1,128,-TMath::Pi(),TMath::Pi());

      }
  }
  /*
  fonll= new TFile("fonll.root");
  if (fonll){
    charm = (TH2D*) fonll->Get("h_charm");
    beauty = (TH2D*) fonll->Get("h_beauty");
  }
  */
}

void RpcAnalysis::execute(std::vector<TrigNtup::Muon> *reco_tracks,
                          std::vector<RpcRoI> *patterns){
    
    
    h_events->Fill(1);
    
    //std::cout << "\n EVENTO " << h_events->GetEntries() << std::endl;	   //
    
    
    // LOOP ON PATTERNS (FOR RATES)
    for(std::vector<RpcRoI>::iterator pat=patterns->begin();
      pat!=patterns->end();pat++){
  
        int thr=0;
        for (int ith=0; ith<nthresholds; ith++){
            if (pat->thrValue>threshold[ith]-0.01
                &&pat->thrValue<threshold[ith]+0.01){
                thr=ith;
            }
        }
        
        for (int iq=0; iq<ntests; iq++){
            if (pat->quality>=iq){
                h_eta_vs_phi_pattern[thr][iq]->Fill(pat->eta,pat->phi);
                h_patterns[thr]->Fill(iq);
            }
        }
        
        h_tower_vs_eta->Fill(pat->eta,pat->trigger_tower);
        h_sector_vs_phi->Fill(pat->phi,pat->trigger_sector);

        if (pat->quality==-2) h_time_BI->Fill(pat->time());
        if (pat->quality==1)  h_time_BIBO->Fill(pat->time());
        if (pat->quality>=2)  h_time->Fill(pat->time());

    }
    
    // Loop on tracks for efficiency 
    for(std::vector<TrigNtup::Muon>::iterator it=reco_tracks->begin();
        it!=reco_tracks->end();it++){

        if (it->quality<=1&&fabs(it->eta)<1.05){ // medium muon in barrel
	  // FILL WITH ALL TRACKS
	  double weight=1.;
          /*
	  if (fonll) {
	    weight=charm->Interpolate(it->eta,it->pt)+
	      beauty->Interpolate(it->eta,it->pt);
	    //std::cout<< weight <<std::endl;
	  }
          */
	  h_oktrks_pt->Fill(it->pt);
	  h_oktrks_pt_w->Fill(it->pt,weight);
	  if (it->pt>25){ 
	    h_oktrks_eta->Fill(it->eta);
	    h_oktrks_etaphi->Fill(it->eta,it->phi);
	    h_oktrks_etaphi_w->Fill(it->eta,it->phi,weight);
	  }
          
	  std::vector<int> vec_of_tests(ntests,-1);
            
            
            for(std::vector<RpcRoI>::iterator pat=patterns->begin();
                pat!=patterns->end();pat++){                
                if (fabs(it->eta-pat->eta)<0.4
                    &&deltaphi(it->phi,pat->phi)<0.4){ // match in eta-phi
                    int thr=0; // find threshold 
                    for (int ith=0; ith<nthresholds; ith++){
                        if (pat->thrValue>threshold[ith]-0.01
                            &&pat->thrValue<threshold[ith]+0.01){
                            thr=ith;
                        }
                    }
                    for (int i=0; i<=pat->quality; i++){
                        if (thr>vec_of_tests.at(i)) vec_of_tests.at(i)=thr;
                    }
                    if (pat->oldTrigger()==2){
                       h_oktrks_pt_trig_old->Fill(it->pt);
                       h_oktrks_eta_trig_old->Fill(it->eta);
                       h_oktrks_etaphi_trig_old->Fill(it->eta,it->phi);
                    }
                 }
            }
            
            // Fill numerators for efficiency
            for (int i=0; i<ntests; i++){
                for (int j=0; j<=vec_of_tests.at(i); j++){
                    h_oktrks_pt_trig[j][i]->Fill(it->pt);
                    h_oktrks_pt_trig_w[j][i]->Fill(it->pt,weight);
                    if (it->pt>25){
                        h_oktrks_eta_trig[j][i]->Fill(it->eta);
                        h_oktrks_etaphi_trig[j][i]->Fill(it->eta,it->phi);
			h_oktrks_etaphi_w->Fill(it->eta,it->phi,weight); 
                    }
                }
            }
        }
    }  // END LOOP ON TRACKS
}

void RpcAnalysis::finalize(){

  
  float events_all= h_events->GetEntries();
  float count_all = h_oktrks_eta->GetEntries();
  
  std::cout << " ------- Summary from RpcAnalysis ----------- "<<std::endl;
  std::cout << "Total events               = " << events_all << std::endl;   
  std::cout << "Total good tracks          = " << count_all << std::endl; 

  float count = 0;
  
  for (int j=0; j<nthresholds; j++) {
      std::cout << " ------- THRESHOLD "<< j <<" = " << threshold[j] <<" GeV --- "<<std::endl;
  
      for (int i=0; i<ntests; i++){
          count=h_oktrks_eta_trig[j][i]->GetEntries();
          std::cout << "Quality >= "<< i << " eff = "<< count/count_all << std::endl; 
      }
  
      for (int i=0; i<ntests; i++){
          count=h_patterns[j]->GetBinContent(i+1);
          std::cout << "Num of patterns per event , quality>="<< i << "  = "<< count/events_all << " +/- " << pow(count,0.5)/events_all << std::endl;
      }
  }

  /* if (fonll) fonll->Close(); */
  
  TFile f("out.root","RECREATE");

  h_events->Write();

  h_oktrks_pt->Write();
  h_oktrks_eta->Write();
  h_oktrks_etaphi->Write();

  h_oktrks_pt_trig_old->Write();
  h_oktrks_eta_trig_old->Write();
  h_oktrks_etaphi_trig_old->Write();

  h_oktrks_pt_w->Write();
  h_oktrks_etaphi_w->Write();
  h_tower_vs_eta->Write();
  h_sector_vs_phi->Write();

  h_time->Write();
  h_time_BIBO->Write();
  h_time_BI->Write();

  for (int j=0; j<nthresholds; j++){
      h_patterns[j]->Write();
      for (int i=0; i<ntests; i++){
          h_oktrks_pt_trig[j][i]->Write();
          h_oktrks_eta_trig[j][i]->Write();
          h_oktrks_etaphi_trig[j][i]->Write();
          h_oktrks_pt_trig_w[j][i]->Write();  
          h_oktrks_etaphi_trig_w[j][i]->Write();  
          h_eta_vs_phi_pattern[j][i]->Write();
      }
  }

  f.Close();

} 

double RpcAnalysis::deltaphi(double phi1,double phi0){
  
  double dphi=phi1-phi0;
  if (dphi>TMath::Pi()) dphi=dphi-2*TMath::Pi();
  if (dphi<-TMath::Pi()) dphi=dphi+2*TMath::Pi();
  
  return fabs(dphi);
  
}
 
