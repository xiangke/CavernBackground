// Code for hit rate in MDTs . In this ADD the infromation about the good run lumi for the runs you want to run
// run on two stream zb = zerobias and ex express stream means contianing empty bunches
// by default is zb
#include <iomanip>
#include <iostream>
#include <string>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include "TCanvas.h"
#include "MuTrigNtExampleAna/ExampleLooper.h"
#include "MuTrigNtExampleAna/RpcAnalysis.h"
#include "TFile.h"

using namespace std;
using namespace TrigNtup;

/*--------------------------------------------------------------------------------*/
// ExampleLooper Constructor
/*--------------------------------------------------------------------------------*/
ExampleLooper::ExampleLooper(TChain *c, TString outputfilename, TString dataset) {
    m_input_chain = c;
    n_readin = 0;
    n_total = 0;
    dataset_ = dataset;
    std::cout<< "dataset_____ = " <<dataset_<<std::endl;
    f = new TFile(outputfilename,"RECREATE");
}
/*--------------------------------------------------------------------------------*/
// The Begin() function is called at the start of the query.
/*--------------------------------------------------------------------------------*/
void ExampleLooper::Begin(TTree* /*tree*/) {
    MuonTriggerNtAna::Begin(0);
    if (m_dbg) std::cout << "ExampleLooper::Begin\n";
    
    if (dataset_=="ex"){
      trigTools.init("/afs/cern.ch/user/x/xiangke/user.xiangke.17438814.EXT0._000156.muTrigNt.root");
    }

    mdt_eta = -99;
    mdt_phi = -99;
    rho = -99;
     
    livetime = 900 * 1e-9; //in seconds 
    length = 100; //cm
    diameter = 3; //cm


    hm_avgMu = new TH1F ("hm_avgMu","hm_avgMu",200,0.,200.);

    hm_lumi =  new TH1F("hm_lumi" ,"hm_lumi",25000,0.,25000.);    
    hm_time = new TH1F("hm_time","hm_time",1000,-1000,4000);  
    hm_etaphi = new TH2F("hm_etaphi","hm_etaphi",17,-8.5,8.5,9,0.5,8.5);
    hm_adc = new TH1F ("hm_adc","hm_adc",1000, -50,1500);    
    hm_rz = new TH2F("hm_rz","hm_rz", 500,-25,25,200,0,20);
    hm_summary_S = new TH2F ("hm_summary_S","hm_summary_S",17,-8.5,8.5,7,0.5,7.5); 
    hm_summary_L = new TH2F ("hm_summary_L","hm_summary_L",17,-8.5,8.5,7,0.5,7.5); 
    hm_event =new TH1F ("hm_events","hm_events",7,0,7);

  etasta_.clear();
  etasta_.push_back(-8);    etasta_.push_back(-7);    etasta_.push_back(-6);    etasta_.push_back(-5);   etasta_.push_back(-4);    etasta_.push_back(-3);    etasta_.push_back(-2);    etasta_.push_back(-1);   etasta_.push_back(1);     etasta_.push_back(2);     etasta_.push_back(3);     etasta_.push_back(4);    etasta_.push_back(5);    etasta_.push_back(6);    etasta_.push_back(7);    etasta_.push_back(8);

    dtype_.clear();
    dtype_.push_back("BIS");
    dtype_.push_back("BMS");
    dtype_.push_back("BOS");
    dtype_.push_back("EIS");
    dtype_.push_back("EMS");
    dtype_.push_back("EOS");
    dtype_.push_back("EES");
    dtype_.push_back("BIL");
    dtype_.push_back("BML");
    dtype_.push_back("BOL");
    dtype_.push_back("EIL");
    dtype_.push_back("EML");
    dtype_.push_back("EOL");
    dtype_.push_back("EEL");

    VarBin_.clear();
    VarBin_[dtype_[0]]  ={0.,200.,400.,600.,720.};
    VarBin_[dtype_[1]]  ={0.,200.,450.,850.,950.};
    VarBin_[dtype_[2]]  ={0.,400.,800.,1200.};
    VarBin_[dtype_[3]]  ={220.,320.,450.,600.};
    VarBin_[dtype_[4]]  ={180.,280.,500.,1000.};
    VarBin_[dtype_[5]]  ={300.,400.,600.,1200.};
    VarBin_[dtype_[6]]  ={220.,320.,450.,600.};          
    VarBin_[dtype_[7]]  ={0.,200.,400.,600.,720.};
    VarBin_[dtype_[8]]  ={0.,200.,450.,850.,950.};
    VarBin_[dtype_[9]]  ={0.,400.,800.,1200.};
    VarBin_[dtype_[10]]  ={220.,320.,450.,600.};
    VarBin_[dtype_[11]]  ={180.,280.,500.,1000.};
    VarBin_[dtype_[12]]  ={300.,400.,600.,1200.};
    VarBin_[dtype_[13]]  ={220.,320.,450.,600.};          

    std::vector<TH1F*> tmp1(etasta_.size(),0);   
    hitrateVsLumi_chamber_eta_.assign(dtype_.size(),tmp1);
    TString hname;

    for (int id=0; id<(int)dtype_.size() ; id++){
      for (int il = 0;il<10; il++){status_[id][il] = false;}

    hm_etaphi_[id] = new TH2F("hm_etaphi_"+dtype_[id], "hm_etaphi_"+dtype_[id], 17,-8.5,8.5,8,0.5,8.5);
    hm_eta_[id] = new TH1F("hm_eta_"+dtype_[id], "hm_eta_"+dtype_[id], 17,-8.5,8.5);
    hitrateVsZ_Full_[id] = new TH1F("hitrateVsZ_Full_"+dtype_[id],"hitrateVsZ_Full_",100,0.,1200.);

    const int nele = (int) VarBin_[dtype_[id]].size();
    Float_t a_arr[nele];

      for (int i=0; i<(int) nele; i++){a_arr[i] = VarBin_[dtype_[id]][i];}

      hitrateVsZ_[id] = new TH1F("hitrateVsZ_"+dtype_[id],"hitrateVsZ_"+dtype_[id],nele-1,a_arr);
      hitrateVsLumi_chamber_[id] = new TH1F("hitrateVsLumi_chamber_"+dtype_[id],"hitrateVsLumi_chamber_"+dtype_[id],25000,0.,25000);
      hitrateVsLumi_chamber_withoutSA_[id] = new TH1F("hitrateVsLumi_chamber_withoutSA_"+dtype_[id],"hitrateVsLumi_chamber_withoutSA_"+dtype_[id],25000,0.,25000);
      
    }
    obj.initialize();
    obj.SetArea();

}

Bool_t ExampleLooper::CheckLSQuality(unsigned int run_, unsigned int lumisection, bool ismc_){

 Bool_t isgood = false;
 if (ismc_ == true){isgood  =true;}
////////////////17data 13TeV good run list good lumi section////////////////////////////////////////  

 if(run_ == 331875){
    if ((lumisection >=71 && lumisection <=479)
       || (lumisection>=481 && lumisection<=677)                                    
       || (lumisection>=679 && lumisection<=765)                                     
       || (lumisection>=767 && lumisection<=1086)){                                     
      isgood  =true;
     }   
 }

 if(run_ == 332915){
    if ((lumisection >=142 && lumisection <=253)
       || (lumisection>=266 && lumisection<=319)                                    
       || (lumisection>=327 && lumisection<=489)                                     
       || (lumisection>=497 && lumisection<=500)){                                     
      isgood  =true;
     }   
 }

 if(run_ == 332304){
    if ((lumisection >=12 && lumisection <=55)
       || (lumisection>=57 && lumisection<=304)                                    
       || (lumisection>=306 && lumisection<=364)){                                     
      isgood  =true;
     }   
 }

 if(run_ == 332953){
    if ((lumisection >=110 && lumisection <=237)
       || (lumisection>=247 && lumisection<=414)                                    
       || (lumisection>=423 && lumisection<=470)){                                     
      isgood  =true;
     }   
 }

 if(run_ == 333181){
    if ((lumisection >=232 && lumisection <=636)){
      isgood  =true;
     }   
 }
 if(run_ == 335290){
    if ((lumisection >=59 && lumisection <=379)){
      isgood  =true;
     }   
 }
 if(run_ == 332896){
    if ((lumisection >=81 && lumisection <=204)){
      isgood  =true;
     }   
 }
  
  return isgood;
}

/*--------------------------------------------------------------------------------*/
// Main process loop function 
/*--------------------------------------------------------------------------------*/
Bool_t ExampleLooper::Process(Long64_t entry) {
  
    GetEntry(entry);
    MuonTriggerNtAna::clearObjects();

    std::cout<<"entry="<<entry<<std::endl; 

    m_chainEntry++;
    bool isMC_ = nt.evt()->isMC;
    unsigned int thisLS = nt.evt()->lb ;
    std::cout<<"lumisection ="<< nt.evt()->lb<<std::endl;

    bool isGoodLS = CheckLSQuality(nt.evt()->run, thisLS,isMC_);
    std::cout<<"check if is good run list"<<isGoodLS<<std::endl;

    if (isMC_){nt.evt()->lbAvgLumi=12500; } // for MC sample by default putting avg lumi value6000

    if (m_dbg || m_chainEntry % 10000 == 0) {
      std::cout << "\n**** Processing entry " << std::setw(6) << m_chainEntry
		<< " run " << std::setw(6)   << nt.evt()->run
		<< " event " << std::setw(7) << nt.evt()->eventNumber 
		<< " av lum "<< std::setw(7) << nt.evt()->lbAvgLumi
		<< " lumisection = "<< std::setw(7) << nt.evt()->lb
		<< "bcid " <<std::setw(7) << nt.evt()->lbBcidLumi
		<< "bunches "<<std::setw(7)  << nt.evt()->bunches
	        << "pile up in " <<std::setw(7)  << nt.evt()->avgMu
		<< " ****\n";
    }
    
    bool pass_HLT_noalg_L1RD0_EMPTY = false;
    bool pass_HLT_noalg_idmon_L1RD0_EMPTY = false;

    if (dataset_=="ex"){
    std::cout<<"dataset = ex"<<std::endl;
    std::cout<<"trigbits ="<<nt.evt()->trigBits<<std::endl;
  //  pass_HLT_noalg_L1RD0_EMPTY =  trigTools.passTrigger(nt.evt()->trigBits, "HLT_noalg_L1RD0_EMPTY");
   pass_HLT_noalg_idmon_L1RD0_EMPTY  = trigTools.passTrigger(nt.evt()->trigBits, "HLT_noalg_idmon_L1RD0_EMPTY");
  //  std::cout<<"pass_HLT_noalg_L1RD0_EMPTY = "<<pass_HLT_noalg_L1RD0_EMPTY<<std::endl;
    std::cout<<"pass_HLT_noalg_idmon_L1RD0_EMPTY = "<<pass_HLT_noalg_idmon_L1RD0_EMPTY<<std::endl;
    }

    if (!isMC_ && isGoodLS){
      //std::cout << " yes pass the triggers" << std::endl;
     if (dataset_=="ex"){
         n_total++;
     	if(pass_HLT_noalg_idmon_L1RD0_EMPTY){      
          std::cout<<"dataset=ex and with trigger pass_HLT_noalg_idmon_L1RD0_EMPTY"<<std::endl;
        if(nt.evt()->avgMu > 26. && nt.evt()->avgMu < 27.)
          {
	  ++n_readin;
	  std::cout << "n_readin"<<n_readin<<std::endl;
	  std::cout << "lbAvgLumi="<<nt.evt()->lbAvgLumi<<std::endl;
	  hm_lumi->Fill(nt.evt()->lbAvgLumi);
	  hm_avgMu->Fill(nt.evt()->avgMu);
	  dumpMdtHit();
          }
     	}
      }

     if(dataset_=="zb"){
        n_total++;
        if(nt.evt()->avgMu > 26. && nt.evt()->avgMu < 27.)
        {
	++n_readin;
        std::cout<<"zb n_readin= "<<n_readin<<std::endl;
	hm_lumi->Fill(nt.evt()->lbAvgLumi);
	hm_avgMu->Fill(nt.evt()->avgMu);
	dumpMdtHit();
        }
      }
    }

    if (isMC_){
	++n_readin;
	hm_lumi->Fill(nt.evt()->lbAvgLumi);
	hm_avgMu->Fill(nt.evt()->avgMu);
	dumpMdtHit();
    }

    return kTRUE;
}

/*--------------------------------------------------------------------------------*/
// The Terminate() function is the last function to be called
/*--------------------------------------------------------------------------------*/
void ExampleLooper::Terminate()
{
  MuonTriggerNtAna::Terminate();
  if(m_dbg) std::cout << "ExampleLooper::Terminate\n";
  
  hm_event->SetBinContent(1,n_readin);
    
    for (int id=0; id<(int)dtype_.size() ; id++){
    double denominator =  livetime*0.01; //0.01 for cm
    
    hm_etaphi_[id]->Scale(1./denominator);
    hm_eta_[id]->Scale(1./denominator);
    hitrateVsLumi_chamber_[id]->Scale(1./denominator);
    hitrateVsLumi_chamber_withoutSA_[id]->Scale(1./livetime);
    //hitrateVsLumi_chamber_[id]->Scale(1./livetime);
    hitrateVsZ_[id]->Scale(1./(denominator));      
    hitrateVsZ_Full_[id]->Scale(1./(denominator));      
    
    int totalcells =0;
    for(int iieta =0; iieta< (int)etasta_.size(); iieta++){          
      for( int iiphi = 1; iiphi <=8; iiphi++){
	float area = obj.getarea(dtype_[id],etasta_[iieta],iiphi);
	if (area > 0.){
	  totalcells = totalcells+1 ;
	}
      }
    }

    hitrateVsZ_[id]->Scale(1./(totalcells));      
    hitrateVsZ_Full_[id]->Scale(1./(totalcells));      
    hm_etaphi_[id]->Scale(1./(totalcells));
    hm_eta_[id]->Scale(1./(totalcells));

    for(int nb = 1; nb <= hm_eta_[id]->GetNbinsX(); nb++){
      int xlab = nb-9;
      int ylab = id+1;
      float rate = hm_eta_[id]->GetBinContent(nb);       
      if(id <= 6) { 
	hm_summary_S->Fill(xlab,ylab,rate);
      }
      if(id > 6) { 
	ylab = id-6  ;
	hm_summary_L->Fill(xlab,ylab,rate);
      }
    }
    
  }
  dumpEventCounters();
  
  f->cd();
  
  for (int id=0; id<(int)dtype_.size() ; id++){
    hm_etaphi_[id]->Write();
    hm_eta_[id]->Write();
    hitrateVsZ_[id]->Write();
    hitrateVsZ_Full_[id]->Write();
    hitrateVsLumi_chamber_[id]->Write();
    hitrateVsLumi_chamber_withoutSA_[id]->Write();
  }
  
  hm_summary_S->Write();
  hm_summary_L->Write();
  hm_etaphi->Write();
  hm_adc->Write();
  hm_time->Write();
  hm_rz->Write();
  hm_lumi->Write();
  hm_avgMu->Write();
  hm_event->Write();
  f->Close();
}

/*--------------------------------------------------------------------------------*/
// Event counters
/*--------------------------------------------------------------------------------*/
void ExampleLooper::dumpEventCounters(){
              std::cout << "\n"
              << "ExampleLooper event counters\n"
              << "read in : " << n_readin << "\n"
              << "total event = : " << n_total << "\n";
}

/*--------------------------------------------------------------------------------*/
// Debug event
/*--------------------------------------------------------------------------------*/
bool ExampleLooper::debugEvent()
{
    return false;
}

/*--------------------------------------------------------------------------------*/
// Dump Offline muon
/*--------------------------------------------------------------------------------*/
void ExampleLooper::dumpMuon()
{
    for(uint iMu = 0; iMu < nt.muo()->size(); ++iMu) {
       TrigNtup::Muon* mu = &nt.muo()->at(iMu); 
       mu->print();
    }
             
}

/*--------------------------------------------------------------------------------*/
// Dump MDT hits info
/*--------------------------------------------------------------------------------*/
void ExampleLooper::dumpMdtHit(){

    std::cout<< " In function dumpMdtHit()"<< std::endl;
    TVector3 v1; 

    for (uint iMdtHit = 0; iMdtHit < nt.mdtHit()->size(); ++iMdtHit) {

      MdtHit* mdtHit = &nt.mdtHit()->at(iMdtHit);

      if (mdtHit->adc < 80 ) continue;

      float zrange = mdtHit->globalPos.Z()/10.;
rho =sqrt((mdtHit->globalPos.X()*mdtHit->globalPos.X())+(mdtHit->globalPos.Y()*mdtHit->globalPos.Y()));

      if (m_dbg)std::cout<< "No. of MDT hit: "<<nt.mdtHit()->size()  <<std::endl;
      if (m_dbg) std::cout << "z :" <<mdtHit->globalPos.Z() << "rho: "  << rho <<std::endl;
if(m_dbg) std::cout <<"Eta:"<<mdt_eta <<"Phi:"<<mdt_phi<<"On segment:"<<mdtHit->isOnSegment<<std::endl;

      v1.SetXYZ(mdtHit->globalPos.X(),mdtHit->globalPos.Y(),mdtHit->globalPos.Z());
      mdt_eta = v1.Eta();
      mdt_phi = v1.Phi();
     
      hm_time->Fill(mdtHit->driftTime);
      hm_etaphi->Fill(mdtHit->stationEta(),mdtHit->stationPhi());
      hm_adc->Fill(mdtHit->adc);
      hm_rz->Fill(mdtHit->globalPos.Z()/1000.,rho/1000.);
      
      TString stationname = mdtHit->stationNameString().substr(0,3);

      if (m_dbg) cout <<" stationame" <<stationname <<std::endl;
      float weight_area = 1./(obj.getarea(stationname,mdtHit->stationEta(),mdtHit->stationPhi()));
          
      for (int id=0; id<(int)dtype_.size() ; id++){

	if(mdtHit->stationNameString().substr(0,3)==dtype_[id]){

	  hm_etaphi_[id]->Fill(mdtHit->stationEta(),mdtHit->stationPhi(),weight_area);
          hm_eta_[id]->Fill(mdtHit->stationEta(),weight_area);
	  hitrateVsLumi_chamber_[id]->Fill(nt.evt()->lbAvgLumi,weight_area);
	  hitrateVsLumi_chamber_withoutSA_[id]->Fill(nt.evt()->lbAvgLumi);

          if(mdtHit->stationNameString().substr(0,1)=="B"){
	    hitrateVsZ_[id]->Fill(fabs(zrange),weight_area);
 	    hitrateVsZ_Full_[id]->Fill(fabs(zrange),weight_area);}

          if(mdtHit->stationNameString().substr(0,1)=="E"){
	    hitrateVsZ_[id]->Fill(fabs(rho/10.),weight_area);
	    hitrateVsZ_Full_[id]->Fill(fabs(rho/10.),weight_area);}
	}
      }
    }
}

/*--------------------------------------------------------------------------------*/
// Dump MDT Segment info
/*--------------------------------------------------------------------------------*/
void ExampleLooper::dumpMdtSegment()
{
    for (uint iMdtSeg = 0; iMdtSeg < nt.mdtSegment()->size(); ++iMdtSeg) {

        Segment* mdtSeg = &nt.mdtSegment()->at(iMdtSeg);

        if(mdtSeg->nMu()==0) continue; //Not attached to offline muon

        if (m_dbg) std::cout << "Segment " << iMdtSeg << " nMu " << mdtSeg->nMu()
                  << " from muon " << mdtSeg->muIdx[0]
                  << " isMDT " << mdtSeg->isMDT 
                  << " isCSC " << mdtSeg->isCSC
                  << " nMdtHit " << mdtSeg->nHits() << std::endl;

        std::cout << " Hits Idx "; 
        for (uint iHit = 0; iHit < mdtSeg->hitsIdx.size(); ++iHit) {
            std::cout <<  mdtSeg->hitsIdx[iHit] << " ";
        }
        std::cout << std::endl;
    }    
}

