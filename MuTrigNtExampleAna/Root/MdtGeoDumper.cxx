#include <iomanip>
#include <iostream>
#include <string>


#include "TCanvas.h"
#include "MuTrigNtExampleAna/MdtGeoDumper.h"
#include "MuTrigNtExampleAna/RpcAnalysis.h"


using namespace std;
using namespace TrigNtup;

/*--------------------------------------------------------------------------------*/
// MdtGeoDumper Constructor
/*--------------------------------------------------------------------------------*/
MdtGeoDumper::MdtGeoDumper(TChain *c) {
    m_input_chain = c;
    n_readin = 0;

    char *tmparea = getenv("ROOTCOREBIN");
    if (tmparea != NULL) {
        m_data_dir = tmparea;
        m_data_dir = m_data_dir + "/data/";
    } else {
        std::cout << " RootCore area not set up " << std::endl
                  << "Exiting... "<< std::endl << std::endl;
        exit(1);
    }

}

/*--------------------------------------------------------------------------------*/
// The Begin() function is called at the start of the query.
/*--------------------------------------------------------------------------------*/
void MdtGeoDumper::Begin(TTree* /*tree*/) {
    MuonTriggerNtAna::Begin(0);
    if (m_dbg) std::cout << "MdtGeoDumper::Begin\n";
      
}

/*--------------------------------------------------------------------------------*/
// Main process loop function 
/*--------------------------------------------------------------------------------*/
Bool_t MdtGeoDumper::Process(Long64_t entry) {
    // Communicate tree entry number to MuonTriggerNtObject
    GetEntry(entry);
    MuonTriggerNtAna::clearObjects();
    ++n_readin;

    // Chain entry not the same as tree entry
    //static Long64_t chainEntry = -1;
    m_chainEntry++;
    if (m_dbg || m_chainEntry % 1000 == 0) {
        std::cout << "\n**** Processing entry " << std::setw(6) << m_chainEntry
                  << " ****\n";
    }

    ////////////////////////////////////////////////////////////////////////////
    // Add any event cuts here; return `kFALSE` if event doesn't pass a cut
    ////////////////////////////////////////////////////////////////////////////

    for (uint iMdtChamb = 0; iMdtChamb < ntMdtGeo.mdtChamberGeo()->size(); ++iMdtChamb) {
        MdtChamberGeo* mdtChamber = &ntMdtGeo.mdtChamberGeo()->at(iMdtChamb);
        if(!m_mdtChamber.empty() &&  mdtChamber->chamberName != m_mdtChamber) continue; 
        mdtChamber->print();
        for(auto iMdtTube : mdtChamber->tubesIdx){
            MdtTube* tube =  &ntMdtGeo.mdtTube()->at(iMdtTube);
            tube->print();
        }
        
    }

    ////////////////////////////////////////////////////////////////////////////
    // If outputting a tree/ntuple:
    // 1. Populate objects/values with info from this event
    // 2. Fill output tree with info from this event
    ////////////////////////////////////////////////////////////////////////////
   

    return kTRUE;
}

/*--------------------------------------------------------------------------------*/
// The Terminate() function is the last function to be called
/*--------------------------------------------------------------------------------*/
void MdtGeoDumper::Terminate()
{
    MuonTriggerNtAna::Terminate();
    if(m_dbg) std::cout << "MdtGeoDumper::Terminate\n";

    dumpEventCounters();
}

/*--------------------------------------------------------------------------------*/
// Event counters
/*--------------------------------------------------------------------------------*/
void MdtGeoDumper::dumpEventCounters()
{
    std::cout << "\n"
              << "MdtGeoDumper event counters\n"
              << "read in : " << n_readin << "\n";
}

/*--------------------------------------------------------------------------------*/
// Debug event
/*--------------------------------------------------------------------------------*/
bool MdtGeoDumper::debugEvent()
{
    return false;
}

