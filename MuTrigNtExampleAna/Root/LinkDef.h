#ifndef MUTRIGNTEXAMPLEANA_LINKDEF_H
#define MUTRIGNTEXAMPLEANA_LINKDEF_H

#include <vector>
#include <map>
#include <string>

#include "MuTrigNtExampleAna/ExampleLooper.h"
#include "MuTrigNtExampleAna/MdtGeoDumper.h"
#include "MuTrigNtExampleAna/RpcAnalysis.h"


#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses; 
#pragma link C++ nestedtypedef;

#pragma link C++ class ExampleLooper;
#pragma link C++ class MdtGeoDumper;


#endif

#endif
