//#pragma once

#ifndef  RpcAnalysis_h
#define  RpcAnalysis_h
//#include "rpcStructs.h"
//#include "trkStructs.h"
#include <TH2.h>
#include <string>
#include "PhaseIIMuonTriggerNtuple/Muon.h"
#include "RpcRoi/RpcRoI.h"

//#include <TH3.h>

class RpcAnalysis{
 public:
  RpcAnalysis();
  ~RpcAnalysis();
  void initialize();
    void execute(std::vector<TrigNtup::Muon> *reco_tracks,
	       std::vector<RpcRoI> *patterns);
  void finalize(); 
  double deltaphi(double phi1,double phi0);  
  
 private:
  //  ALL HISTOGRAMS
  int ntests; // 
  int nthresholds;
  std::vector<float> threshold;  

  TH1F * h_events;
  TH2F * h_tower_vs_eta;
  TH2F * h_sector_vs_phi;

  TH1F * h_oktrks_pt;
  TH1F * h_oktrks_eta;
  TH2F * h_oktrks_etaphi;
  TH1F * h_oktrks_pt_w;
  TH2F * h_oktrks_etaphi_w;

  TH1F * h_time;
  TH1F * h_time_BIBO;
  TH1F * h_time_BI;

  // one per threshold
  std::vector<TH1F *>  h_patterns;  

  // one per threshold and quality
  std::vector<std::vector<TH1F*> > h_oktrks_pt_trig;
  std::vector<std::vector<TH1F*> > h_oktrks_eta_trig;
  std::vector<std::vector<TH2F*> > h_oktrks_etaphi_trig;
  std::vector<std::vector<TH1F*> > h_oktrks_pt_trig_w;
  std::vector<std::vector<TH2F*> > h_oktrks_etaphi_trig_w;
  std::vector<std::vector<TH2F*> > h_eta_vs_phi_pattern;

  TH1F* h_oktrks_pt_trig_old;
  TH1F* h_oktrks_eta_trig_old;
  TH2F* h_oktrks_etaphi_trig_old;
  

  TFile * fonll;
  TH2D * charm;
  TH2D * beauty;
  
};

#endif
